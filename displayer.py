import cmath
import numpy as np
import matplotlib.pyplot as plt
import imageio
from colorspace.colorlib import HCL
import math

def hcl_to_rgb(h, c, l):
	colour = HCL(h, c, l)
	colour.to('RGB')
	return np.array([colour.get('R')[0], colour.get('G')[0], colour.get('B')[0]])	

def to_riemann(Z):
	if math.isnan(Z.real) or math.isinf(Z.real) or math.isnan(Z.imag) or math.isinf(Z.imag):
		return np.array([0, 0, -1])
	z = 1 / (Z.real**2 + Z.imag**2 + 1)
	return np.array([z*Z.real*2, z*Z.imag*2, z*2 - 1])

def to_colour(xyz):
	arg = math.atan2(xyz[1], xyz[0])
	r = (xyz[0]**2 + xyz[1]**2)**0.5
	#return hcl_to_rgb(arg*180/math.pi+180, r*100, (xyz[2]+1)*50)
	return np.array([arg*180/math.pi+180, r*100, (xyz[2]+1)*50])

def iterated_function(z0, c, func, maxiter, escape_dist):
	z = z0
	#s = to_riemann(z)
	for i in range(0, maxiter):
		try:
			z = func(z, c)
		except:
			return np.array([0, 0, 0])
		if abs(z) > escape_dist:
			return np.array([0, 0, 100*math.tanh(i/20)])
			#return hcl_to_rgb(0, 0, 100/maxiter*i)
		#s = s + to_riemann(z)
	#return to_colour(s / (s**2).sum()**0.5)
	return to_colour(to_riemann(z))

def mandelbrot(z, c):
	return z**2 + c

def logobrot(z, c):
	if z == 0:
		return complex(np.nan, np.nan)
	return c*(z**(cmath.log(z)))

def sin2brot(z, c):
	return c*cmath.sin(z)**2

def identity(c):
	return c

def render_plane(plane, z0, func, maxiter, escape_dist):
	img = np.zeros([*(plane.shape), 3])
	numel = plane.shape[0]*plane.shape[1]
	#for y in range(0, plane.shape[0]):
	#	for x in range(0, plane.shape[1]):
	#		img[y, x, :] = iterated_function(z0(plane[y, x]), plane[y, x], func, maxiter, escape_dist)
	img = np.stack([np.stack([iterated_function(z0(plane[y, x]), plane[y, x], func, maxiter, escape_dist) for x in range(0, plane.shape[1])]) for y in range(0, plane.shape[0])])
	colours = HCL(img[:,:,0].reshape([numel]).tolist(), img[:,:,1].reshape([numel]).tolist(), img[:,:,2].reshape([numel]).tolist())
	colours.to('RGB')
	img = np.stack([np.array(colours.get('R')).reshape(plane.shape), np.array(colours.get('G')).reshape(plane.shape), np.array(colours.get('B')).reshape(plane.shape)]).transpose([1, 2, 0])
	img[img<0] = 0
	img[img>1] = 1
	img = (img*255).astype("uint8")
	return img

def flat_plane(re_min, re_max, im_min, im_max, re_res, im_res):
	return np.linspace(re_min, re_max, re_res) + np.linspace(complex(0, im_max), complex(0, im_min), im_res)[np.newaxis].transpose()

def riemann_sphere(resolution):
	t, y = np.meshgrid(np.linspace(-math.pi, math.pi, int(math.pi*resolution)), np.linspace(-0.5, 0.5, resolution))
	r = (0.25-y**2)**0.5
	x = np.sin(t)*r
	z = (1+np.cos(t))*r
	return x/z + y/z*complex(0, 1)

if __name__ == "__main__":
	plane = flat_plane(-2, 2, -2, 2, 1000, 1000)
	img = render_plane(plane, identity, logobrot, 40, 100000)

	imageio.imwrite("test.png", img)
	#print(to_colour(complex(1, 0.00)))
