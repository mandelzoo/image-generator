import numpy as np
import matplotlib.pyplot as plt
import imageio
from scipy.signal import convolve2d
import displayer
import cmath

sigma = 10
gauss = np.exp(-np.linspace(-3, 3, 6*sigma)**2)[np.newaxis]
gauss = gauss/np.sum(gauss)

def gauss_filter(x):
	return convolve2d(convolve2d(x, gauss, "same"), gauss.T, "same")

def deviation(x):
	return gauss_filter(x**2) - gauss_filter(x)**2

def explore_fractal(name, z0, func, escape_dist = 100000, base_scale = 2, res=1000):
	plane = displayer.riemann_sphere(res)
	img = displayer.render_plane(plane, z0, func, 100, escape_dist)
	imageio.imwrite("img/" + name + "_sphere.png", img)
	plane = displayer.flat_plane(-base_scale, base_scale, -base_scale, base_scale, res, res)
	img = displayer.render_plane(plane, z0, func, 100, escape_dist)
	imageio.imwrite("img/" + name + "_overview.png", img)
	grey = 0.2989*img[:,:,0] + 0.5870*img[:,:,1] + 0.1140*img[:,:,2]
	dg = deviation(grey)
	c = plane[np.unravel_index(np.argmax(dg), dg.shape)]
	plane = displayer.flat_plane(c.real-base_scale/10, c.real+base_scale/10, c.imag-base_scale/10, c.imag+base_scale/10, res, res)
	img = displayer.render_plane(plane, z0, func, 400, escape_dist)
	imageio.imwrite("img/" + name + "_detail.png", img)

def multibrot(z, c, n):
	if z == 0:
		return c
	return z**n + c

def logobrot(z, c, n, q):
	if z == 0:
		return complex(np.nan, np.nan)
	return c*(z**(q*cmath.log(z)**n))

def sine_multibrot(z, c, n):
	if z == 0:
		return 0
	return c*cmath.sin(z)**n

def cosine_multibrot(z, c, n):
	if z == 0:
		return 0
	return c*cmath.cos(z)**n

def approx_cosine_multibrot(z, c, n, m):
	if z == 0:
		return 0
	return c*(1 - z**n/n)**m

def approx_sine_multibrot(z, c, n, m):
	if z == 0:
		return 0
	return c*(z - z**n/n)**m

def identity(c):
	return c

def zeros(c):
	return 0

def ones(c):
	return 1

for n in range(2, 6):
	explore_fractal("multibrot_%d"%(n), identity, lambda z, c : multibrot(z, c, n))
	explore_fractal("antimultibrot_%d"%(n), identity, lambda z, c : multibrot(z, c, -n), escape_dist=np.nan)

for n in range(1, 4):
	explore_fractal("sine_multibrot_%d"%(n), identity, lambda z, c : sine_multibrot(z, c, n))
	explore_fractal("cosine_multibrot_%d"%(n), identity, lambda z, c : cosine_multibrot(z, c, n))
	explore_fractal("cosecans_multibrot_%d"%(n), identity, lambda z, c : sine_multibrot(z, c, -n))
	explore_fractal("secans_multibrot_%d"%(n), identity, lambda z, c : cosine_multibrot(z, c, -n), escape_dist=np.nan)

for m in range(1, 4):
	for n in [-3, -2, -1, 2, 3, 4]:
		if n < 0:
			if m == 1:
				explore_fractal("approx_sine_multibrot_%d_%d"%(n, m), ones, lambda z, c : approx_sine_multibrot(z, c, n, m), escape_dist=np.nan)
			else:
				explore_fractal("approx_sine_multibrot_%d_%d"%(n, m), ones, lambda z, c : approx_sine_multibrot(z, c, n, m))
			explore_fractal("approx_cosine_multibrot_%d_%d"%(n, m), identity, lambda z, c : approx_cosine_multibrot(z, c, n, m), escape_dist=np.nan)
		else:
			explore_fractal("approx_sine_multibrot_%d_%d"%(n, m), ones, lambda z, c : approx_sine_multibrot(z, c, n, m), base_scale=3)
			explore_fractal("approx_cosine_multibrot_%d_%d"%(n, m), identity, lambda z, c : approx_cosine_multibrot(z, c, n, m))

for q in [0.5, 0.7, 1]:
	for n in range(1, 6):
		explore_fractal("logobrot_%d_%f"%(n, q), identity, lambda z, c : logobrot(z, c, n, q))

for m in range(1, 4):
	for n in [-3, -2, -1, 2, 3, 4]:
		explore_fractal("approx_sine_multibrot_%d_%d"%(n, -m), ones, lambda z, c : approx_sine_multibrot(z, c, n, -m))
		explore_fractal("approx_cosine_multibrot_%d_%d"%(n, -m), identity, lambda z, c : approx_cosine_multibrot(z, c, n, -m))

